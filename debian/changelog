bumprace (1.5.7-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Add missing colon in closes line.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 31 Oct 2019 13:41:50 +0000

bumprace (1.5.7-1) unstable; urgency=medium

  * new upstream version (closes: #916865)
  * spelling patches are included upstream

 -- Christian T. Steigies <cts@debian.org>  Thu, 07 Feb 2019 22:28:46 +0100

bumprace (1.5.4-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Add Vcs-* field

  [ Bruno "Fuddl" Kleinert ]
  * Add replacements for non-DFSG effect and music files that were removed
    previously (closes: #916865)
    + Store them in debian/sound/ and list them in
      debian/source/include-binaries.
    + Add licensing information to debian/copyright.
    + Add debian/README.source
    + Add load-replaced-sound-files.patch.

  [ Christian T. Steigies ]
  * updated Standards-Version to 4.2.1, remove menu file
  * switch to debhelper 11
  * drop cdbs
  * remove depends on autotools-dev

 -- Christian T. Steigies <cts@debian.org>  Tue, 25 Dec 2018 14:54:47 +0100

bumprace (1.5.4-3) unstable; urgency=medium

  * added a Homepage field, thanks for the patch from Sina Momken
    (closes: #810716)
  * update watch file
  * updated Standards-Version to 3.9.7 (no changes)
  * more spelling corrections
  * removed unnecessary versioned dependencies with "cme fix dpkg-control"

 -- Christian T. Steigies <cts@debian.org>  Wed, 30 Mar 2016 20:51:04 +0200

bumprace (1.5.4-2) unstable; urgency=low

  * provide desktop file (closes: #737837)
  * fix spelling mistakes (closes: #749735)
  * updated Standards-Version to 3.9.5 (no changes)

 -- Christian T. Steigies <cts@debian.org>  Sun, 12 Oct 2014 18:05:19 +0200

bumprace (1.5.4-1) unstable; urgency=low

  * new upstream version, sound and music have been removed (closes: #613344)
  * add libjpeg-dev and zlib1g-dev to Build-Depends (closes: #669525,#672900)
  * remove devscripts from Build-Depends as suggested by Adam D. Barratt
  * updated Standards-Version to 3.9.3 (no changes)

 -- Christian T. Steigies <cts@debian.org>  Mon, 14 May 2012 23:38:14 +0200

bumprace (1.5.3-4) unstable; urgency=low

  * add libjpeg-dev and zlib1g-dev to Build-Depends (closes: #669525)

 -- Christian T. Steigies <cts@debian.org>  Thu, 10 May 2012 22:51:03 +0200

bumprace (1.5.3-3) unstable; urgency=low

  * copyright: refer to GPL-2
  * copyright and origin of sound and music files are given in AUTHORS,
    add contents to copyright file (closes: #613344)
  * add ${misc:Depends} to dependencies
  * updated Standards-Version to 3.9.2 (no changes)
  * install binary in install override target
  * switch to dpkg-source 3.0 (quilt) format

 -- Christian T. Steigies <cts@debian.org>  Sun, 04 Sep 2011 19:25:53 +0200

bumprace (1.5.3-2) unstable; urgency=low

  * updated Standards-Version to 3.7.3, use section Games/Action according
    to the menu policy
  * modify build-depends for libsdl1.2-dev as suggested by lintian
  * re-create manpage with docbook-to-man to remove lintian warnings

 -- Christian T. Steigies <cts@debian.org>  Mon, 24 Mar 2008 19:39:13 +0100

bumprace (1.5.3-1) unstable; urgency=low

  * new upstream version (closes: #409602)

 -- Christian T. Steigies <cts@debian.org>  Sun, 11 Feb 2007 17:16:31 +0100

bumprace (1.5.2-1) unstable; urgency=low

  * new upstream version (closes: #392318)
  * update watch file
  * switch to cdbs
  * remove Build-Depends on imagemagick
  * updated Standards-Version to 3.7.2 (no changes)

 -- Christian T. Steigies <cts@debian.org>  Wed, 27 Dec 2006 17:38:08 +0100

bumprace (1.5.1.dfsg-2.1) unstable; urgency=low

  [ Joey Hess ]
  * NMU with patch from tbm to fix FTBFS with current gcc. Closes: #392318

  [ root ]
  * GNU config automated update: config.sub     (20060223 to 20060920),
    config.guess     (20060223 to 20060702)

 -- Joey Hess <joeyh@debian.org>  Sat, 14 Oct 2006 23:00:39 -0400

bumprace (1.5.1.dfsg-2) unstable; urgency=low

  * add weblink to package description (closes: #348272)
  * use current debhelper
  * updated Standards-Version to 3.7.0 (no changes)
  * bumprace works fine on all AMD64 boxes I have access to (closes: #318249)
    it seems to fail though, if used with the Nvidia binary graphics driver,
    which I can not fix in debian, especially not without help from the bug submitter
  * GNU config automated update: config.sub     (20050708 to 20060223),
    config.guess     (20050803 to 20060223)

 -- Christian T. Steigies <cts@debian.org>  Wed,  3 May 2006 21:47:22 +0000

bumprace (1.5.1.dfsg-1) unstable; urgency=high

  * remove back2.jpg from upstream source (closes: #336227)
  * created background_by_cts.xcf with gimp which can be used as a replacement
    (I am not an artist, if you have a nice, free picture, please contact Karl)
  * background_by_cts.xcf is converted in debian/rules with imagemagick to
    back2.jpg, thus build-depend on imagemagick
  * GNU config automated update: config.sub (20050708 to 20050708),
    config.guess (20050708 to 20050803)

 -- Christian T. Steigies <cts@debian.org>  Sat, 29 Oct 2005 18:24:17 +0000

bumprace (1.5.1-1) unstable; urgency=low

  * new upstream version
  * unapply patches with -N, patch by Karl Chen (closes: #319313)

 -- Christian T. Steigies <cts@debian.org>  Sun, 17 Jul 2005 11:47:46 +0200

bumprace (1.5.0-1) unstable; urgency=low

  * new upstream version which includes most debian patches

 -- Christian T. Steigies <cts@debian.org>  Fri, 15 Jul 2005 09:17:58 +0200

bumprace (1.4.6-6) unstable; urgency=low

  * Fix segfault on ia64, patch by Dann Frazier (closes: #317065)
  * remove warnings about implicit declarations
  * updated Standards-Version to 3.6.2.0, no changes

 -- Christian T. Steigies <cts@debian.org>  Wed,  6 Jul 2005 23:27:48 +0200

bumprace (1.4.6-5) unstable; urgency=low

  * added automatic config.{sub|guess} updating (closes: #316793)
  * GNU config automated update: config.sub (20021130 to 20050422),
    config.guess (20021130 to 20050422)

 -- Christian T. Steigies <cts@debian.org>  Tue,  5 Jul 2005 21:04:29 +0000

bumprace (1.4.6-4) unstable; urgency=high

  * really, really fix buffer overflow by applying patches in configure,
    removing patches in clean target (closes: #302493)

 -- Christian T. Steigies <cts@debian.org>  Wed, 13 Apr 2005 22:50:44 +0200

bumprace (1.4.6-3) unstable; urgency=high

  * fix buffer overflow, again (closes: #290706). The patch was lost,
    along with the changelog entry, when moving to new upstream version
  * fix lintian warnings about menu:
    - add quotes in menu entry
    - menu icons now seem to belong in /usr/share/pixmaps
    - change menu and rules accordingly
  * do not start description of data package with a capital letter
  * fix copyright file

 -- Christian T. Steigies <cts@debian.org>  Sun, 16 Jan 2005 21:05:41 +0100

bumprace (1.4.6-2) unstable; urgency=low

  * added icon (closes: #211822)
  * updated Standards-Version to 3.6.1.0, use DEB_BUILD_OPTIONS

 -- Christian T. Steigies <cts@debian.org>  Wed,  5 Nov 2003 00:03:22 -0500

bumprace (1.4.6-1) unstable; urgency=low

  * new upstream version, includes speedup patch (closes: #198396)
  * increase audio buffer to prevent choppy sound
  * remove "." in description and (s) in copyright to make lintian happy

 -- Christian T. Steigies <cts@debian.org>  Fri, 22 Aug 2003 22:01:34 -0400

bumprace (1.4.5-3) unstable; urgency=low

  * apply patch from Steve Kemp to fix buffer overflow (closes: #203226)

 -- Christian T. Steigies <cts@debian.org>  Mon, 28 Jul 2003 21:52:10 -0400

bumprace (1.4.5-2) unstable; urgency=low

  * rebuild with libpng3-dev (closes: #166268)

 -- Christian T. Steigies <cts@debian.org>  Fri, 25 Oct 2002 10:07:19 -0400

bumprace (1.4.5-1) unstable; urgency=low

  * new upstream version

 -- Christian T. Steigies <cts@debian.org>  Sat, 13 Apr 2002 17:39:40 -0400

bumprace (1.4.4-1) unstable; urgency=low

  * new upstream version
  * add menu again, also add a no-sound menu entry (closes: #138362)

 -- Christian T. Steigies <cts@debian.org>  Tue, 19 Mar 2002 20:47:48 -0500

bumprace (1.4.3-2) unstable; urgency=low

  * bumprace-data: Replaces: bumprace (<< 1.4.3-1) (closes: #128994)

 -- Christian T. Steigies <cts@debian.org>  Sun, 13 Jan 2002 11:57:11 -0500

bumprace (1.4.3-1) unstable; urgency=low

  * new upstream version (closes: #79151)
  * changing maintainer email back to cts to reduce the amount of SPAM
  * update source location in copyright
  * manpage rewritten in sgml and updated for new/changed command-line options
  * cosmetic patch for data/Makefile.am from Karl
  * GNU config automated update: config.sub (20011005 to 20020102),
    config.guess (20011005 to 20020102)

 -- Christian T. Steigies <cts@debian.org>  Tue,  8 Jan 2002 22:06:39 -0500

bumprace (1.4.2-5) unstable; urgency=low

  * do not use automake, autoconf for package building
  * complete overhaul with the help of autotools-dev
  * added autotools-dev, devscripts to Build-Depends
  * use $SDL_LIBS instead of -L/usr/X11R6/lib -lXxf86dga -lXxf86vm -lXv in
    configure.in as recommended by Karl
  * ran ispell over some docs

 -- Christian T. Steigies <cts@debian.org>  Sat, 13 Oct 2001 23:47:26 -0400

bumprace (1.4.2-4) unstable; urgency=low

  * Build-Depends on libsdl1.2-dev (>= 1.2.2-3.1),
    libsdl-image1.2-dev(>= 1.2.0-1.1), libsdl-mixer1.2-dev (>= 1.2.0-1.1)
    due to SDL overhaul
  * configure.in needs to include -L/usr/X11R6/lib -lXxf86dga -lXxf86vm -lXv
    for the SDL_mixer test now

 -- Christian T. Steigies <cts@debian.org>  Wed, 10 Oct 2001 23:26:12 -0400

bumprace (1.4.2-3) unstable; urgency=low

  * build with SDL1.2 packages
  * update Standards-Version
  * do not ship INSTALL

 -- Christian T. Steigies <cts@debian.org>  Tue,  5 Jun 2001 21:15:47 -0400

bumprace (1.4.2-2) unstable; urgency=low

  * remove (unneeded) dh_suidregister from debian/rules

 -- Christian T. Steigies <cts@debian.org>  Sat,  3 Feb 2001 21:15:53 +0100

bumprace (1.4.2-1) unstable; urgency=low

  * new upstream version (closes: #71246)
  * now uses SDL1.1, sdl-image1.1 and sdl-mixer1.1 (closes: #72975)
  * change maintainer address to cts-bumprace@debian.org
    (automatically forward bug reports upstream)

 -- Christian T. Steigies <cts@debian.org>  Mon, 23 Oct 2000 23:54:41 +0200

bumprace (1.4.1-1) unstable; urgency=low

  * SDL1.1 test version

 -- Christian T. Steigies <cts@debian.org>  Thu, 19 Oct 2000 23:51:00 +0200

bumprace (1.4-2) unstable; urgency=low

  * build with SDL1.0, sdl-image1.0, sdl-mixer1.0 (closes: #72975)

 -- Christian T. Steigies <cts@debian.org>  Sat, 14 Oct 2000 23:05:48 +0200

bumprace (1.4-1) unstable; urgency=low

  * new upstream version (closes: #65266, 64240, 64241)
  * added missing build-depends (closes: #65243)
  * changed maintainer address

 -- Christian T. Steigies <cts@debian.org>  Wed, 21 Jun 2000 20:11:36 +0200

bumprace (1.3-3) unstable; urgency=low

  * added more missing build-depends (closes: #65315)

 -- Christian T. Steigies <cts@debian.org>  Tue, 13 Jun 2000 23:38:03 +0200

bumprace (1.3-2) unstable; urgency=low

  * added missing build-depends (closes: #65232)

 -- Christian T. Steigies <cts@debian.org>  Mon,  5 Jun 2000 21:28:38 +0200

bumprace (1.3-1) unstable; urgency=low

  * new upstream version with highscores and a fix for the timeout bug
    (closes: #61620, #61633, #61969)
  * now building with SDL 1.1 (sdl-mixer needs this)
  * use bumpracer-nomixer source, we have an sdl-mixer package.

 -- Christian T. Steigies <cts@debian.org>  Mon, 15 May 2000 21:01:31 +0200

bumprace (1.2-4) unstable; urgency=low

  * recompiled with SDL 1.0.8, README.Debian can go (closes:#61619)

 -- Christian T. Steigies <cts@debian.org>  Mon,  3 Apr 2000 11:25:15 +0200

bumprace (1.2-3) unstable; urgency=low

  * updated the description wth a text from the author.
  * data files go in /usr/share/games not /usr/share

 -- Christian T. Steigies <cts@debian.org>  Sat, 11 Mar 2000 17:48:19 +0100

bumprace (1.2-2) unstable; urgency=low

  * use automake and autoconf to make the diff much smaller
    (and upload this, and not the inital try...)
  * correct some typos in the README

 -- Christian T. Steigies <cts@debian.org>  Thu,  9 Mar 2000 18:58:11 +0100

bumprace (1.2-1) unstable; urgency=low

  * Initial Release.

 -- Christian T. Steigies <cts@debian.org>  Thu,  2 Mar 2000 22:26:52 +0100
